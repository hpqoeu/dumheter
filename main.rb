#!/usr/bin/env ruby
# coding: utf-8
# -*- encoding: utf-8 -*-

require "rubygems"
require "highline/import"
require "mechanize"


class ImpulseBot
	def initialize
		@mechanize = Mechanize.new { |agent|
			agent.user_agent_alias = "Windows IE 6"
			agent.follow_meta_refresh = true
			agent.redirect_ok = true
			agent.read_timeout = 10
		}
		if ARGV.empty?
			@username = ask("Username: ") {|q| q.echo=true }
			@password = ask("Password: ") {|q| q.echo="*" }
		else
			@username = ARGV[0]
			@password = ARGV[1]
		end
	end

	def Login
		login_url = "http://impulse.nu:8080/?sida=login"
		@mechanize.get(login_url)
		form = @mechanize.page.forms.first
		form.anv = @username
		form.pass = @password
		logged_in = @mechanize.submit(form)
	end
	
	def Nyheter
		nyheter_url = "http://impulse.nu:8080/?sida=misc/nyheter"
		nyheter_req = @mechanize.get(nyheter_url)
		puts @mechanize.cookies.to_s
		choose do |menu|
			menu.prompt = "Which job do you want to do?"
			
			menu.choice(:federal_bank) { 
				@jobb_url = "http://impulse.nu:8080/?sida=jobb/olagliga_jobb&q=federal_bank&p=redo&a=&c=0" 
				@jobb_namn = "Federal Bank" }
			menu.choice(:oljeshejk) { 
				@jobb_url = "http://impulse.nu:8080/?sida=jobb/olagliga_jobb&q=oljeshejk&p=redo&a=&c=0" 
				@jobb_namn = "Oljeshejk" }
			menu.choice(:opiumfalt) {
				@jobb_url = "http://impulse.nu:8080/?sida=jobb/olagliga_jobb&q=befast_opiumfalt&p=redo&a=&c=0"
				@jobb_namn = "Befast Opiumfalt" }
		end
	end

	def Level
		energi_url = "http://impulse.nu:8080/?sida=stan/frukter&kop=1"
		@mechanize.get(energi_url)

		jobb_req = @mechanize.get(@jobb_url)
		
		getlevel = jobb_req.body.match(/<div id='stats_lvl'>\d+ \(\d+(\.\d+)?%\)<\/div>/)
		getlevel = getlevel[0].gsub!(/<\/?div.*?>/, "")
		
		gethealth = jobb_req.body.match(/<div id='stats_liv'>\d+\/\d+<\/div>/)
		gethealth = gethealth[0].gsub!(/<\/?div.*?>/, "")
		
		getenergy = jobb_req.body.match(/<div id='stats_energi'>\d+\/\d+<\/div>/)
		getenergy = getenergy[0].gsub!(/<\/?div.*?>/, "")
		
		getcash = jobb_req.body.match(/<div id='stats_cash'>((\d+ )?\d+ )?\d+ kr<\/div>/)
		getcash = getcash[0].gsub!(/<\/?div.*?>/, "").gsub!(/ kr/, "")
		
		cashute = getcash.gsub(/ /, "").to_i
		
		health = gethealth.split("/")[0].to_i	
		maxhealth = gethealth.split("/")[1].to_i


		if health <= (maxhealth*0.85)
			@mechanize.get("http://impulse.nu:8080/?sida=stan/sjukhuset&a=aterstall")
			puts "Filled health."

		elsif cashute >= 1000000
			@mechanize.get("http://impulse.nu:8080/?sida=stan/stan&bankomat=in&q=")			

			puts "Deposited"

		else
			puts "-*-"

		end
			
		if m = jobb_req.body.match(/klampat i klaveret\!/)
			puts "Trial!"

		elsif m = jobb_req.body.match(/Min cell/)
			puts "Jail!"
			@mechanize.get("http://impulse.nu:8080/?sida=vakter&q=fri")
			
		elsif m = jobb_req.body.match(/Vad vill du.*/)
			puts "Fled from robbery!"
			@mechanize.get("http://impulse.nu:8080/?Attackera&q=fly")

		else
			puts "Did #{@jobb_namn}"
		end
			
		puts "H: #{gethealth} E: #{getenergy} L: #{getlevel} $: #{getcash}"
		
	end
end

a = ImpulseBot.new
a.Login
a.Nyheter
while true do
	a.Level
	sleep(0.8)
end
